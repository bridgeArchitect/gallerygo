package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/labstack/gommon/log"
	"html/template"
	"net/http"
	"strconv"
	"strings"
)

/* path to images and html-pages */
const (
	pathMainPage string = "/home/bridgearchitect/gallerygo/mainPage.html"
	pathGalleryPage string = "/home/bridgearchitect/gallerygo/galleryPage.html"
	pathPicturePage string = "/home/bridgearchitect/gallerygo/picturePage.html"
    pathMountRImages string = "/home/bridgearchitect/gallerygo/mountR/"
	pathMountFImages string = "/home/bridgearchitect/gallerygo/mountF/"
	pathMunRImages string = "/home/bridgearchitect/gallerygo/munR/"
	pathMunFImages string = "/home/bridgearchitect/gallerygo/munF/"
	pathSeaRImages string = "/home/bridgearchitect/gallerygo/seaR/"
	pathSeaFImages string = "/home/bridgearchitect/gallerygo/seaF/"
	pathUkrRImages string = "/home/bridgearchitect/gallerygo/ukrR/"
	pathUkrFImages string = "/home/bridgearchitect/gallerygo/ukrF/"
)

/* true record of gallery */
type galleryRecord struct {
	Id          int
	Number      int
	Description string
}

/* real record of gallery in database */
type galleryRecordDB struct {
	id   int
	name string
}

/* real record of picture */
type pictureRecord struct {
	Id         int
	IdGal      int
	Name       string
	RedPath    string
	FullPath   string
	PictNumber int
}

/* global variables to work */
var (
	database    *sql.DB         /* object to work with database */
	gallRecords []galleryRecord /* gallery records */
	pictRecords []pictureRecord /* picture records */
	pictRecord   pictureRecord  /* one picture record */
)

func handleError(err error) {

	/* handle error */
	if err != nil {
		log.Fatal(err)
	}

}

func giveMainPage() *template.Template {

	var (
		err          error              /* variable for error */
		rows         *sql.Rows          /* rows of answer */
		currRecordDB galleryRecordDB    /* current record of database */
		currRecord   galleryRecord      /* correct current record */
		subrows      []string           /* substrings of "name" field */
		template     *template.Template /* template of html-page */
	)

	/* make query to database */
	rows, err = database.Query("select * from galleryArray")
	handleError(err)

	/* receive arrays of records */
	gallRecords = make([]galleryRecord, 0, 100)
	for rows.Next() {

		/* read one record */
		err = rows.Scan(&currRecordDB.id, &currRecordDB.name)
		handleError(err)

		/* parse one record */
		currRecord.Id = currRecordDB.id
		subrows = strings.Split(currRecordDB.name, ":")
		currRecord.Number, err = strconv.Atoi(subrows[0])
		handleError(err)
		currRecord.Description = subrows[1]

		/* save one record */
		gallRecords = append(gallRecords, currRecord)

	}

	/* generate main page */
	template, err = template.ParseFiles(pathMainPage)
	handleError(err)
	return template

}

func mainPageHandle(writer http.ResponseWriter, request *http.Request) {

	var (
		template *template.Template /* template of main html-page */
		err      error              /* handle error */
	)

	/* receive main page */
	template = giveMainPage()
	/* give main page for client */
	err = template.Execute(writer, gallRecords)
	handleError(err)

}

func giveGalleryPage(request *http.Request) *template.Template {

	var (
		id          string             /* id of gallery */
		vars        map[string]string  /* variables from ulr-query */
		rows        *sql.Rows          /* rows of sql-query */
		err         error              /* variable for error */
		currRecord  pictureRecord      /* current record */
		template    *template.Template /* template of html-page */
	)

	/* read variables of query */
	vars = mux.Vars(request)
	id = vars["id"]

	/* make query to database */
	rows, err = database.Query("select * from pictureArray where idGal = ?", id)
	handleError(err)

	/* receive arrays of records */
	pictRecords = make([]pictureRecord, 0, 100)
	for rows.Next() {

		/* read one record */
		err = rows.Scan(&currRecord.Id, &currRecord.IdGal, &currRecord.Name, &currRecord.RedPath, &currRecord.FullPath, &currRecord.PictNumber)
		handleError(err)

		/* handle this record */
		currRecord.RedPath = currRecord.RedPath[9:]
		currRecord.FullPath = currRecord.FullPath[9:]

		/* save one record */
		pictRecords = append(pictRecords, currRecord)

	}

	/* generate gallery page */
	template, err = template.ParseFiles(pathGalleryPage)
	handleError(err)
	return template

}

func galleryPageHandle(writer http.ResponseWriter, request *http.Request) {

	var (
		template *template.Template /* template of gallery html-page */
		err      error              /* variable for error */
	)

	/* receive gallery page */
	template = giveGalleryPage(request)
	/* give gallery page for client */
	err = template.Execute(writer, pictRecords)
	handleError(err)

}

func pictureGalleryPage(request *http.Request) *template.Template {

	var (
		id          string             /* id of gallery */
		vars        map[string]string  /* variables from ulr-query */
		rows        *sql.Rows          /* rows of sql-query */
		err         error              /* variable for error */
		template    *template.Template /* template of html-page */
	)

	/* read variables of query */
	vars = mux.Vars(request)
	id = vars["id"]

	/* make query to database */
	rows, err = database.Query("select * from pictureArray where id = ?", id)
	handleError(err)

	/* read one record */
	rows.Next()
	err = rows.Scan(&pictRecord.Id, &pictRecord.IdGal, &pictRecord.Name, &pictRecord.RedPath, &pictRecord.FullPath, &pictRecord.PictNumber)
	handleError(err)

	/* handle this record */
	pictRecord.RedPath = pictRecord.RedPath[9:]
	pictRecord.FullPath = pictRecord.FullPath[9:]

	/* generate gallery page */
	template, err = template.ParseFiles(pathPicturePage)
	handleError(err)
	return template

}

func picturePageHandle(writer http.ResponseWriter, request *http.Request) {

	var (
		template *template.Template /* template of gallery html-page */
		err      error              /* variable for error */
	)

	/* receive picture page */
	template = pictureGalleryPage(request)
	/* give picture page for client */
	err = template.Execute(writer, pictRecord)
	handleError(err)

}

func main() {

	var (
		err    error       /* variable for error */
		router *mux.Router /* router for web pages */
	)

	/* open database */
	database, err = sql.Open("mysql", "go:Moskow2012@/gallery")
	handleError(err)
	defer database.Close()

	/* create router for web pages and add handlers */
	router = mux.NewRouter()
	router.HandleFunc("/", mainPageHandle)
	router.HandleFunc("/gallery/{id:[0-9]+}", galleryPageHandle).Methods("GET")
	router.HandleFunc("/picture/{id:[0-9]+}", picturePageHandle).Methods("GET")

	/* turn on handling (dynamic files) */
	http.Handle("/", router)
	/* turn on handling (static files) */
	http.Handle("/mountR/",http.StripPrefix("/mountR/", http.FileServer(http.Dir(pathMountRImages))))
	http.Handle("/mountF/",http.StripPrefix("/mountF/", http.FileServer(http.Dir(pathMountFImages))))
	http.Handle("/munR/",http.StripPrefix("/munR/", http.FileServer(http.Dir(pathMunRImages))))
	http.Handle("/munF/",http.StripPrefix("/munF/", http.FileServer(http.Dir(pathMunFImages))))
	http.Handle("/seaR/",http.StripPrefix("/seaR/", http.FileServer(http.Dir(pathSeaRImages))))
	http.Handle("/seaF/",http.StripPrefix("/seaF/", http.FileServer(http.Dir(pathSeaFImages))))
	http.Handle("/ukrR/",http.StripPrefix("/ukrR/", http.FileServer(http.Dir(pathUkrRImages))))
	http.Handle("/ukrF/",http.StripPrefix("/ukrF/", http.FileServer(http.Dir(pathUkrFImages))))

	/* launch http-server */
	fmt.Println("Server listening...")
	http.ListenAndServe(":15000", nil)

}
